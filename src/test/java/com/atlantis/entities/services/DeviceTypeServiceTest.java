package com.atlantis.entities.services;


import com.atlantis.entities.models.*;
import com.atlantis.entities.services.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestEntityManager
@TestPropertySource(locations="classpath:test.properties")
@Sql(
        value = "classpath:db/migration/java-db.sql",
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
)
@Transactional
public class DeviceTypeServiceTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DeviceTypeService deviceTypeService;

    @Test
    public void findByName()
    {
        DeviceType deviceType = new DeviceType();
        deviceType.setName("t1");
        UUID id = UUID.randomUUID();
        deviceType.setId(id);
        entityManager.persist(deviceType);

        DeviceType found = deviceTypeService.getOne(id);
        assertThat(found.getName()).isEqualTo("t1");

        deviceType.setName("t2");
        entityManager.persist(deviceType);

        DeviceType found2 = deviceTypeService.getOne(id);
        assertThat(found2.getName()).isEqualTo("t2");

        DeviceType foundByName = deviceTypeService.findOneByName("t2");
        assertThat(foundByName.getName()).isEqualTo("t2");
    }
}
