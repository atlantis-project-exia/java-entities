package com.atlantis.entities.services;

import com.atlantis.entities.models.*;
import com.atlantis.entities.services.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestEntityManager
@TestPropertySource(locations="classpath:test.properties")
@Sql(
        value = "classpath:db/migration/java-db.sql",
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
)
@Transactional
public class DeviceServiceTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DeviceService deviceService;

    @Test
    public void save() {
        DeviceType deviceType = new DeviceType("test");
        entityManager.persist(deviceType);
        Device device = new Device("test", deviceType);
        entityManager.persist(device);

        Device found = deviceService.getOne(device.getId());

        assertThat(found.getName()).isEqualTo(device.getName());
    }

    @Test
    public void update() {
        DeviceType deviceType = new DeviceType("test");
        entityManager.persist(deviceType);
        Device device = new Device("test", deviceType);
        entityManager.persist(device);

        Device found = deviceService.getOne(device.getId());

        assertThat(found.getName()).isEqualTo(device.getName());

        found.setName("t2");
        entityManager.persist(found);
        Device found2 = deviceService.getOne(device.getId());

        assertThat(found2.getName()).isEqualTo("t2");
    }

    @Test
    public void createOrUpdate() {
        DeviceType deviceType = new DeviceType("test");
        entityManager.persist(deviceType);

        List<Device> emptyList = new ArrayList<>();

        Group group = new Group("gr1", emptyList);
        entityManager.persist(group);
        UUID id = UUID.randomUUID();

        Map<String, Object> data = new HashMap<>();
        data.put("device_type", deviceType.getId().toString());
        data.put("name", "t1");
        data.put("group", group.getId().toString());

        Device device = new Device();
        device.setId(id);
        entityManager.persist(
                deviceService.createOrUpdate(device, data)
        );

        Device found = deviceService.getOne(id);
        assertThat(found.getName()).isEqualTo("t1");
        assertThat(found.getDevice_type().getId()).isEqualTo(deviceType.getId());

        List<Group> groups = found.getGroups();
        assertThat(groups.size()).isEqualTo(1);

        assertThat(groups.get(0).getId()).isEqualTo(group.getId());
    }
}

