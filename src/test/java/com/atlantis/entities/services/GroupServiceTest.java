package com.atlantis.entities.services;

import com.atlantis.entities.models.*;
import com.atlantis.entities.services.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestEntityManager
@TestPropertySource(locations="classpath:test.properties")
@Sql(
        value = "classpath:db/migration/java-db.sql",
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
)
@Transactional
public class GroupServiceTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private GroupService groupService;

    @Test
    public void createOrUpdate()
    {
        List<Device> emptyList = new ArrayList<>();
        Map<String, Object> data = new HashMap<>();
        data.put("name", "t1");

        UUID id = UUID.randomUUID();

        Group group = new Group();
        group.setId(id);
        entityManager.persist(groupService.createOrUpdate(group, data));

        Group found = groupService.getOne(id);

        assertThat(found.getName()).isEqualTo("t1");
    }
}
