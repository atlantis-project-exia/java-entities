DROP TABLE IF EXISTS users;
CREATE TABLE users
(
    user_id_users    UUID NOT NULL,
    first_name_users VARCHAR(200),
    last_name_users  VARCHAR(200),
    email_users      VARCHAR(200),
    is_admin_users   BOOLEAN,
    password_users   VARCHAR(200),
    tokens_users     VARCHAR(200),
    created_at_users TIMESTAMP,
    PRIMARY KEY (user_id_users)
);
DROP TABLE IF EXISTS devices;
CREATE TABLE devices
(
    uid_devices  UUID NOT NULL,
    name_devices VARCHAR(200),
    device_type_id_device_type UUID,
    PRIMARY KEY (uid_devices)
);
DROP TABLE IF EXISTS device_type;
CREATE TABLE device_type
(
    device_type_id_device_type UUID NOT NULL,
    name_device_type           VARCHAR(200),
    PRIMARY KEY (device_type_id_device_type)
);
DROP TABLE IF EXISTS metrics;
CREATE TABLE metrics
(
    metric_id_metrics  UUID NOT NULL,
    value_metrics      BYTEA,
    created_at_metrics TIMESTAMP,
    metric_type_id_metric_type UUID,
    uid_devices UUID,
    PRIMARY KEY (metric_id_metrics)
);
DROP TABLE IF EXISTS metric_type;
CREATE TABLE metric_type
(
    metric_type_id_metric_type UUID NOT NULL,
    name_metric_type           VARCHAR(200),
    unit_metric_type           VARCHAR(200),
    type_metric_type           VARCHAR(200),
    PRIMARY KEY (metric_type_id_metric_type)
);
DROP TABLE IF EXISTS groups;
CREATE TABLE groups
(
    group_id_group UUID NOT NULL,
    name_group     VARCHAR(200),
    PRIMARY KEY (group_id_group)
);
DROP TABLE IF EXISTS posseder;
CREATE TABLE posseder
(
    user_id_users UUID NOT NULL,
    group_id_group UUID NOT NULL,
    PRIMARY KEY (user_id_users, group_id_group)
);
DROP TABLE IF EXISTS rassembler;
CREATE TABLE rassembler
(
    group_id_group UUID NOT NULL,
    uid_devices UUID NOT NULL,
    PRIMARY KEY (group_id_group, uid_devices)
);
ALTER TABLE devices
    ADD CONSTRAINT FK_devices_device_type_id_device_type FOREIGN KEY (device_type_id_device_type) REFERENCES device_type (device_type_id_device_type);
ALTER TABLE metrics
    ADD CONSTRAINT FK_metrics_metric_type_id_metric_type FOREIGN KEY (metric_type_id_metric_type) REFERENCES metric_type (metric_type_id_metric_type);
ALTER TABLE metrics
    ADD CONSTRAINT FK_metrics_uid_devices FOREIGN KEY (uid_devices) REFERENCES devices (uid_devices);
ALTER TABLE posseder
    ADD CONSTRAINT FK_posseder_user_id_users FOREIGN KEY (user_id_users) REFERENCES users (user_id_users);
ALTER TABLE posseder
    ADD CONSTRAINT FK_posseder_group_id_group FOREIGN KEY (group_id_group) REFERENCES groups (group_id_group);
ALTER TABLE rassembler
    ADD CONSTRAINT FK_rassembler_group_id_group FOREIGN KEY (group_id_group) REFERENCES groups (group_id_group);
ALTER TABLE rassembler
    ADD CONSTRAINT FK_rassembler_uid_devices FOREIGN KEY (uid_devices) REFERENCES devices (uid_devices);
