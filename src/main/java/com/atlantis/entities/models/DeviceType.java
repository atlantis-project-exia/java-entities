package com.atlantis.entities.models;


import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Table(name="device_type")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DeviceType {
    @Id
    @Column(name = "device_type_id_device_type", updatable = false, nullable = false)
    private UUID id;

    @Column(name="name_device_type")
    private String name;

    public DeviceType() {
        if (id == null) {
            id = UUID.randomUUID();
        }
    }

    public DeviceType(String name) {
        this();
        this.name = name;
    }
}
