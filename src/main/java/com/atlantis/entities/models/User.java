package com.atlantis.entities.models;


import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;


@Entity
@Table(name="users")
@Getter
@Setter
@JsonIgnoreProperties({"password", "hibernateLazyInitializer", "handler"})
public class User {
    @Id
    @Column(name = "user_id_users", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "first_name_users")
    private String first_name;

    @Column(name = "last_name_users")
    private String last_name;

    @Column(name = "email_users")
    private String email;

    @Column(name = "is_admin_users")
    private Boolean is_admin;

    @Column(name = "password_users")
    private String password;

    @Column(name = "tokens_users")
    private String tokens;

    @Column(name="created_at_users")
    private LocalDateTime created_at;

    @ManyToMany(cascade=CascadeType.REMOVE)
    @JoinTable(
            name = "posseder",
            joinColumns = @JoinColumn(name = "user_id_users"),
            inverseJoinColumns = @JoinColumn(name = "group_id_group")
    )
    @JsonManagedReference
    List<Group> groups = new ArrayList<>();

    public User() {
        if (id == null) {
            id = UUID.randomUUID();
        }
    }

    public User(String first_name, String last_name, String email, Boolean is_admin, String password, String tokens, LocalDateTime created_at, List<Group> groups) {
        this();
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.is_admin = is_admin;
        this.password = password;
        this.tokens = tokens;
        this.created_at = created_at;
        this.groups = groups;
    }
}
