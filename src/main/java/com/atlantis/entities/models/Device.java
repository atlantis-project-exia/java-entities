package com.atlantis.entities.models;


import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;


@Entity
@Table(name="devices")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter
@Setter
public class Device {
    @Id
    @Column(name = "uid_devices", updatable = false, nullable = false)
    private UUID id;

    @Column(name="name_devices")
    private String name;

    @OneToOne(cascade=CascadeType.REMOVE)
    @JoinColumn(name = "device_type_id_device_type", referencedColumnName = "device_type_id_device_type")
    private DeviceType device_type;

    @ManyToMany(cascade=CascadeType.REMOVE, mappedBy = "devices")
    @JsonBackReference
    List<Group> groups = new ArrayList<>();

    public Device() {
        if (id == null) {
            id = UUID.randomUUID();
        }
    }

    public Device(String name, DeviceType device_type) {
        this();
        this.name = name;
        this.device_type = device_type;
    }
}
