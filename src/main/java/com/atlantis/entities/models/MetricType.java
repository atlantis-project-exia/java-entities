package com.atlantis.entities.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Table(name="metric_type")
@Getter
@Setter
public class MetricType {
    @Id
    @Column(name = "metric_type_id_metric_type", updatable = false, nullable = false)
    private UUID id;

    @Column(name="name_metric_type")
    private String name;

    @Column(name="unit_metric_type")
    private String unit;

    @Column(name="type_metric_type")
    private String type;

    public MetricType() {
        if (id == null) {
            id = UUID.randomUUID();
        }
    }

    public MetricType(String name, String unit, String type) {
        this();
        this.name = name;
        this.unit = unit;
        this.type = type;
    }
}
