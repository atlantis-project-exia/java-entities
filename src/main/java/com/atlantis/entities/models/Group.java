package com.atlantis.entities.models;


import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;


@Entity
@Table(name="groups")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Group {
    @Id
    @Column(name = "group_id_group", updatable = false, nullable = false)
    private UUID id;

    @Column(name="name_group")
    private String name;

    @ManyToMany(cascade=CascadeType.REMOVE)
    @JoinTable(
            name = "rassembler",
            joinColumns = @JoinColumn(name = "group_id_group"),
            inverseJoinColumns = @JoinColumn(name = "uid_devices")
    )
    @JsonManagedReference
    List<Device> devices = new ArrayList<>();

    @ManyToMany(cascade=CascadeType.REMOVE, mappedBy = "groups")
    @JsonBackReference
    List<User> users = new ArrayList<>();


    public Group() {
        if (id == null) {
            id = UUID.randomUUID();
        }
    }

    public Group(String name, List<Device> devices) {
        this();
        this.name = name;
        this.devices = devices;
    }
}
