package com.atlantis.entities.models;

import com.atlantis.entities.GenericValue;
import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;


@Entity
@Table(name="metrics")
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Metric {
    @Id
    @Column(name = "metric_id_metrics", updatable = false, nullable = false)
    private UUID id;

    @Column(name="created_at_metrics")
    private LocalDateTime created_at;

    @Column(name="value_metrics")
    @Type(type="org.hibernate.type.BinaryType")
    private byte[] value;

    @ManyToOne(cascade=CascadeType.REMOVE)
    @JoinColumn(name = "metric_type_id_metric_type", referencedColumnName = "metric_type_id_metric_type")
    private MetricType metric_type;

    @ManyToOne(cascade=CascadeType.REMOVE)
    @JoinColumn(name = "uid_devices", referencedColumnName = "uid_devices")
    private Device device;

    public GenericValue getVal() {
        return GenericValue.fromBinary(this.value, this.metric_type.getType());
    }
    public void setVal(GenericValue value) {
        this.value = GenericValue.toBinary(value, this.metric_type.getType());
    }

    public Metric() {
        if (id == null) {
            id = UUID.randomUUID();
        }
    }

    public Metric(LocalDateTime created_at, byte[] value, MetricType metric_type, Device device_type) {
        this();
        this.created_at = created_at;
        this.value = value;
        this.metric_type = metric_type;
        this.device = device;
    }
}
