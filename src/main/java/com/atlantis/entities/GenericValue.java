package com.atlantis.entities;


import java.nio.charset.StandardCharsets;

public class GenericValue<T> {
    private T value;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public static GenericValue fromBinary(byte[] value, String type) {
        switch (type) {
            case "int":
                GenericValue<Integer> rint = new GenericValue<>();

                rint.setValue(Byte.valueOf(value[0]).intValue());
                return rint;
            case "float":
                GenericValue<Float> rfloat = new GenericValue<>();
                rfloat.setValue(Byte.valueOf(value[0]).floatValue());
                return rfloat;
            case "bool":
                GenericValue<Boolean> rbool = new GenericValue<>();
                rbool.setValue(value[0] != 0);
                return rbool;
            case "string":
                GenericValue<String> rstring = new GenericValue<>();
                rstring.setValue(new String(value, StandardCharsets.UTF_8));
                return rstring;
            default:
                throw new IllegalStateException("type :'"+type+"' is not recognized !");
        }
    }

    public static byte[] toBinary(GenericValue value, String type) {
        switch (type) {
            case "int":
                int vint = (int) value.getValue();
                return new byte[]{(byte) vint};
            case "float":
                float vfloat = (float) value.getValue();
                return new byte[]{(byte) vfloat};
            case "bool":
                boolean vbool = (boolean) value.getValue();
                return new byte[]{(byte) (vbool ? 1 : 0 )};
            case "string":
                String vstring = (String) value.getValue();
                return vstring.getBytes(StandardCharsets.UTF_8);
            default:
                throw new IllegalStateException("type :'"+type+"' is not recognized !");
        }
    }
}
