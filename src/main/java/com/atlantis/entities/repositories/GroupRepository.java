package com.atlantis.entities.repositories;


import com.atlantis.entities.models.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface GroupRepository extends JpaRepository<Group, UUID> {
}
