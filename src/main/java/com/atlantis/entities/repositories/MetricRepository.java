package com.atlantis.entities.repositories;


import com.atlantis.entities.models.Device;
import com.atlantis.entities.models.Metric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.UUID;


@Repository
public interface MetricRepository extends JpaRepository<Metric, UUID> {
    public List<Metric> findAllByDevice(Device device, Pageable pageable);
}
