package com.atlantis.entities.repositories;


import com.atlantis.entities.models.MetricType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface MetricTypeRepository extends JpaRepository<MetricType, UUID> {
    MetricType findOneByName(String name);
}
