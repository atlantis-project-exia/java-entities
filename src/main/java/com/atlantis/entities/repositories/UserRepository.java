package com.atlantis.entities.repositories;


import com.atlantis.entities.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    public User findOneByTokens(String token);
    public Optional<User> findOneByEmail(String email);
}
