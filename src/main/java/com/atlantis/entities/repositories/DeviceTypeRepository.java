package com.atlantis.entities.repositories;


import com.atlantis.entities.models.DeviceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface DeviceTypeRepository extends JpaRepository<DeviceType, UUID> {
    DeviceType findOneByName(String name);
}
