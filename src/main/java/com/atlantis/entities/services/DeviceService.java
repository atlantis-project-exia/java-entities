package com.atlantis.entities.services;


import com.atlantis.entities.models.*;
import com.atlantis.entities.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.UUID;


@Component
public class DeviceService {

    @Autowired
    private DeviceRepository repository;

    @Autowired
    private DeviceTypeRepository deviceTypeRepository;

    @Autowired
    private GroupRepository groupRepository;

    public List<Device> findAll() {
        return repository.findAll();
    }

    public List<Device> findAllById(List<UUID> ids) {
        return repository.findAllById(ids);
    }

    public Device save(Device device) {
        return repository.save(device);
    }

    public Device getOne(UUID id) {
        return repository.getOne(id);
    }

    public void delete(Device device) {
        repository.delete(device);
    }

    public void deleteById(UUID id) {
        repository.deleteById(id);
    }

    public Device createOrUpdate(Device device, Map<String, Object> body) {
        if (body.containsKey("device_type")) {
            DeviceType deviceType = deviceTypeRepository.getOne(UUID.fromString((String) body.get("device_type")));
            device.setDevice_type(deviceType);
        }
        if (body.containsKey("name")) {
            device.setName((String) body.get("name"));
        }
        if (body.containsKey("group")) {
            Group group = groupRepository.getOne(UUID.fromString((String) body.get("group")));
            device.getGroups().add(group);
        }
        device = repository.save(device);
        return device;

    }
}