package com.atlantis.entities.services;

import com.atlantis.entities.models.Group;
import com.atlantis.entities.models.User;
import com.atlantis.entities.repositories.GroupRepository;
import com.atlantis.entities.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;


@Component
public class UserService {
    @Autowired
    private UserRepository repository;

    @Autowired
    private GroupService groupService;

    public List<User> findAll() {
        return repository.findAll();
    }

    public User save(User user) {
        return repository.save(user);
    }

    public User getOne(UUID id) {
        return repository.getOne(id);
    }

    public Optional<User> getOneByEmail(String email) {
        return repository.findOneByEmail(email);
    }

    public void delete(User user) {
        repository.delete(user);
    }

    public void deleteById(UUID id) {
        repository.deleteById(id);
    }

    public User getOneByToken(String token) {
        return repository.findOneByTokens(token);
    }

    public User createOrUpdateDirect(User user) {
        user = this.save(user);
        return user;
    }

    public User createOrUpdate(User user, Map<String, Object> body) {
        if (body.containsKey("first_name")) {
            user.setFirst_name((String) body.get("first_name"));
        }
        if (body.containsKey("last_name")) {
            user.setLast_name((String) body.get("last_name"));
        }
        if (body.containsKey("email")) {
            user.setEmail((String) body.get("email"));
        }
        if (body.containsKey("tokens")) {
            user.setTokens((String) body.get("tokens"));
        }
        if (body.containsKey("is_admin")) {
            user.setIs_admin((Boolean) body.get("is_admin"));
        } else {
            user.setIs_admin(false);
        }
        if (body.containsKey("password")) {
            user.setPassword((String) body.get("password"));
        }
        if (body.containsKey("group")) {
            Group group = groupService.getOne(UUID.fromString((String) body.get("group")));
            user.getGroups().add(group);
        }
        user = repository.save(user);
        return user;
    }
}
