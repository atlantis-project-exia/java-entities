package com.atlantis.entities.services;

import com.atlantis.entities.models.MetricType;
import com.atlantis.entities.repositories.MetricTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;


@Component
public class MetricTypeService {
    @Autowired
    private MetricTypeRepository repository;

    public List<MetricType> findAll() {
        return repository.findAll();
    }

    public MetricType save(MetricType metric) {
        return repository.save(metric);
    }

    public MetricType getOne(UUID id) {
        return repository.getOne(id);
    }

    public void delete(MetricType metric) {
        repository.delete(metric);
    }

    public void deleteById(UUID id) {
        repository.deleteById(id);
    }

    public MetricType findOneByName(String name) {
        return repository.findOneByName(name);
    }
}
