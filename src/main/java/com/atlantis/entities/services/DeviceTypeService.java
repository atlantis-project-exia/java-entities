package com.atlantis.entities.services;


import com.atlantis.entities.models.DeviceType;
import com.atlantis.entities.repositories.DeviceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;


@Component
public class DeviceTypeService {

    @Autowired
    private DeviceTypeRepository repository;

    public List<DeviceType> findAll() {
        return repository.findAll();
    }

    public DeviceType save(DeviceType device) {
        return repository.save(device);
    }

    public DeviceType getOne(UUID id) {
        return repository.getOne(id);
    }

    public void delete(DeviceType device) {
        repository.delete(device);
    }

    public void deleteById(UUID id) {
        repository.deleteById(id);
    }

    public DeviceType findOneByName(String name) {
        return repository.findOneByName(name);
    }
}