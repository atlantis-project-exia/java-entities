package com.atlantis.entities.services;

import com.atlantis.entities.models.Device;
import com.atlantis.entities.models.Metric;
import com.atlantis.entities.repositories.MetricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;


@Component
public class MetricService {
    @Autowired
    private MetricRepository repository;

    public List<Metric> findAll() {
        return repository.findAll();
    }

    public List<Metric> findByDevicePaginated(Device device, Pageable pageable) {
        return repository.findAllByDevice(device, pageable);
    }

    public Metric save(Metric metric) {
        return repository.save(metric);
    }

    public Metric getOne(UUID id) {
        return repository.getOne(id);
    }

    public void delete(Metric metric) {
        repository.delete(metric);
    }

    public void deleteById(UUID id) {
        repository.deleteById(id);
    }
}
