package com.atlantis.entities.services;

import com.atlantis.entities.models.Group;
import com.atlantis.entities.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.UUID;


@Component
public class GroupService {
    @Autowired
    private GroupRepository repository;

    public List<Group> findAll() {
        return repository.findAll();
    }

    public List<Group> findAllById(List<UUID> ids) {
        return repository.findAllById(ids);
    }

    public Group save(Group group) {
        return repository.save(group);
    }

    public Group getOne(UUID id) {
        return repository.getOne(id);
    }

    public void delete(Group group) {
        repository.delete(group);
    }

    public void deleteById(UUID id) {
        repository.deleteById(id);
    }

    public Group createOrUpdate(Group group, Map<String, Object> body) {
        if (body.containsKey("name")) {
            group.setName((String) body.get("name"));
        }
        group = repository.save(group);
        return group;

    }
}
